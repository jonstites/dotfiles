local ret_status="%(?:%{$fg_bold[green]%}➜ :%{$fg_bold[red]%}➜ %s)"
reset="%{$reset_color%}"


PROMPT='${ret_status}%{$FG[064]%}%n${reset} %{$FG[006]%}%M${reset} %{$FG[225]%}%~${reset} %{$fg_bold[blue]%}$(git_prompt_info)${reset}
╰─ %{$fg[white]%}%# %{$reset_color%}%'
RPROMPT="[%D{%Y-%m-%d %H:%M:%S}]"

ZSH_THEME_GIT_PROMPT_PREFIX="git:(%{$fg[red]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[blue]%}) %{$fg[yellow]%}✗%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg[blue]%})"
